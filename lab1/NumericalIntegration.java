import java.util.function.Function;

public abstract class NumericalIntegration {
    protected int numPoints; // число точек
    protected double step; // шаг
    protected double precision; // точность

    public NumericalIntegration(int numPoints, double precision) {
        if (numPoints <= 1) { // проверяем корректность числа точек
            throw new IllegalArgumentException("Число точек должно быть больше 1");
        }
        if (precision <= 0) { // проверяем корректность точности
            throw new IllegalArgumentException("Точность должна быть положительным числом");
        }
        this.numPoints = numPoints;
        this.precision = precision;
        this.step = 1.0 / (numPoints - 1);
    }

    public abstract double calc(Function<Double, Double> f, double lowerBound, double upperBound);

}