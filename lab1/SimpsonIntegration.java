import java.util.function.Function;

public class SimpsonIntegration extends NumericalIntegration {
    public SimpsonIntegration(int numPoints, double precision) {
        super(numPoints, precision);
    }

    protected double simpsonRule(Function<Double, Double> f, double lowerBound, double upperBound) {
        double sum = f.apply(lowerBound) + f.apply(upperBound);
        for (int i = 1; i < numPoints - 1; i += 2) {
            double x = lowerBound + i * step;
            sum += 4 * f.apply(x);
        }
        for (int i = 2; i < numPoints - 1; i += 2) {
            double x = lowerBound + i * step;
            sum += 2 * f.apply(x);
        }
        return sum * step / 3;
    }

    @Override
    public double calc(Function<Double, Double> f, double lowerBound, double upperBound) {
        double integral = simpsonRule(f, lowerBound, upperBound);
        double error = Math.abs(integral - simpsonRule(f, lowerBound, (lowerBound + upperBound) / 2)
                - simpsonRule(f, (lowerBound + upperBound) / 2, upperBound)) / 15;
        while (error > precision) {
            numPoints *= 2;
            step /= 2;
            integral = simpsonRule(f, lowerBound, upperBound);
            error = Math.abs(integral - simpsonRule(f, lowerBound, (lowerBound + upperBound) / 2)
                    - simpsonRule(f, (lowerBound + upperBound) / 2, upperBound)) / 15;
        }
        return integral;
    }
}
