import java.util.function.Function;

public class TrapezoidalIntegration extends NumericalIntegration {
    public TrapezoidalIntegration(int numPoints, double precision) {
        super(numPoints, precision);
    }

    @Override
    public double calc(Function<Double, Double> f, double lowerBound, double upperBound) {
        double integral = trapezoidalRule(f, lowerBound, upperBound);
        double error = Math.abs(integral - trapezoidalRule(f, lowerBound, (lowerBound + upperBound) / 2)
                - trapezoidalRule(f, (lowerBound + upperBound) / 2, upperBound)) / 3;
        while (error > precision) {
            numPoints *= 2;
            step /= 2;
            integral = trapezoidalRule(f, lowerBound, upperBound);
            error = Math.abs(integral - trapezoidalRule(f, lowerBound, (lowerBound + upperBound) / 2)
                    - trapezoidalRule(f, (lowerBound + upperBound) / 2, upperBound)) / 3;
        }
        return integral;
    }

    protected double trapezoidalRule(Function<Double, Double> f, double lowerBound, double upperBound) {
        double step = (upperBound - lowerBound) / numPoints;
        double sum = 0.5 * (f.apply(lowerBound) + f.apply(upperBound));
        for (int i = 1; i < numPoints - 1; i++) {
            double x = lowerBound + i * step;
            sum += f.apply(x);
        }
        return sum * step;
    }
}