public class Main {
    public static void main(String[] args) {
        // создание экземпляра
        Array3d arr = new Array3d(3, 4, 5);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 5; k++) {
                    arr.set(i, j, k, i + j + k);
                }
            }
        }

        int[] slice = arr.getValues0(1);
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 5; k++) {
                System.out.print(slice[j * 5 + k] + " ");
            }
            System.out.println();
        }

        int[][] values = {{1, 2, 3, 4, 5}};
        arr.setValues01(2, 3, values);

        slice = arr.getValues01(2, 3);
        for (int k = 0; k < 5; k++) {
            System.out.print(slice[k] + " ");
        }
        System.out.println();

        // создание массива с (ones)
        Array3d ones = Array3d.ones(2, 2, 2);
        System.out.println("Массив с (ones):");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    System.out.print(ones.get(i, j, k) + " ");
                }
                System.out.println();
            }
            System.out.println();
        }

        // создание массива с (zeros)
        Array3d zeros = Array3d.zeros(2, 2, 2);
        System.out.println("Массив с (zeros):");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    System.out.print(zeros.get(i, j, k) + " ");
                }
                System.out.println();
            }
            System.out.println();
        }

        // создание массива с (fill)
        Array3d fill = Array3d.fill(2, 2, 2, 5);
        System.out.println("Массив с (fill):");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                for (int k = 0; k < 2; k++) {
                    System.out.print(fill.get(i, j, k) + " ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

}