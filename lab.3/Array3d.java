public class Array3d {
private int dim0, dim1, dim2;
    private int[] arr;

    public Array3d(int d0, int d1, int d2) {
        dim0 = d0;
        dim1 = d1;
        dim2 = d2;
        arr = new int[dim0 * dim1 * dim2];
    }

    public int getDim0() {
        return dim0;
    }

    public int getDim1() {
        return dim1;
    }

    public int getDim2() {
        return dim2;
    }

    public int get(int i, int j, int k) {
        return arr[i * dim1 * dim2 + j * dim2 + k];
    }

    public void set(int i, int j, int k, int value) {
        arr[i * dim1 * dim2 + j * dim2 + k] = value;
    }

    public int[] getValues0(int i) {
        int[] slice = new int[dim1 * dim2];
        for (int j = 0; j < dim1; j++) {
            for (int k = 0; k < dim2; k++) {
                slice[j * dim2 + k] = arr[i * dim1 * dim2 + j * dim2 + k];
            }
        }
        return slice;
    }

    public int[] getValues1(int j) {
        int[] slice = new int[dim0 * dim2];
        for (int i = 0; i < dim0; i++) {
            for (int k = 0; k < dim2; k++) {
                slice[i * dim2 + k] = arr[i * dim1 * dim2 + j * dim2 + k];
            }
        }
        return slice;
    }

    public int[] getValues2(int k) {
        int[] slice = new int[dim0 * dim1];
        for (int i = 0; i < dim0; i++) {
            for (int j = 0; j < dim1; j++) {
                slice[i * dim1 + j] = arr[i * dim1 * dim2 + j * dim2 + k];
            }
        }
        return slice;
    }

    public int[] getValues01(int i, int j) {
        int[] slice = new int[dim2];
        for (int k = 0; k < dim2; k++) {
            slice[k] = arr[i * dim1 * dim2 + j * dim2 + k];
        }
        return slice;
    }

    public int[] getValues02(int i, int k) {
        int[] slice = new int[dim1];
        for (int j = 0; j < dim1; j++) {
            slice[j] = arr[i * dim1 * dim2 + j * dim2 + k];
        }
        return slice;
    }

    public int[] getValues12(int j, int k) {
        int[] slice = new int[dim0];
        for (int i = 0; i < dim0; i++) {
            slice[i] = arr[i * dim1 * dim2 + j * dim2 + k];
        }
        return slice;
    }

    public void setValues0(int i, int[][] values) {
        for (int j = 0; j < dim1; j++) {
            for (int k = 0; k < dim2; k++) {
                arr[i * dim1 * dim2 + j * dim2 + k] = values[j][k];
            }
        }
    }

    public void setValues1(int j, int[][] values) {
        for (int i = 0; i < dim0; i++) {
            for (int k = 0; k < dim2; k++) {
                arr[i * dim1 * dim2 + j * dim2 + k] = values[i][k];
            }
        }
    }

    public void setValues2(int k, int[][] values) {
        for (int i = 0; i < dim0; i++) {
            for (int j = 0; j < dim1; j++) {
                arr[i * dim1 * dim2 + j * dim2 + k] = values[i][j];
            }
        }
    }

    public void setValues01(int i, int j, int[][] values) {
        for (int k = 0; k < dim2; k++) {
            arr[i * dim1 * dim2 + j * dim2 + k] = values[0][k];
        }
    }

    public void setValues02(int i, int k, int[][] values) {
        for (int j = 0; j < dim1; j++) {
            arr[i * dim1 * dim2 + j * dim2 + k] = values[0][j];
        }
    }

    public void setValues12(int j, int k, int[][] values) {
        for (int i = 0; i < dim0; i++) {
            arr[i * dim1 * dim2 + j * dim2 + k] = values[0][i];
        }
    }

    public static Array3d ones(int d0, int d1, int d2) {
        Array3d arr = new Array3d(d0, d1, d2);
        for (int i = 0; i < d0; i++) {
            for (int j = 0; j < d1; j++) {
                for (int k = 0; k < d2; k++) {
                    arr.set(i, j, k, 1);
                }
            }
        }
        return arr;
    }

    public static Array3d zeros(int d0, int d1, int d2) {
        return new Array3d(d0, d1, d2);
    }

    public static Array3d fill(int d0, int d1, int d2, int value) {
        Array3d arr = new Array3d(d0, d1, d2);
        for (int i = 0; i < d0; i++) {
            for

            (int j = 0; j < d1; j++) {
                for (int k = 0; k < d2; k++) {
                    arr.set(i, j, k, value);
                }
            }
        }
        return arr;
    }
}
