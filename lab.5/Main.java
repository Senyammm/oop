
import java.util.Vector;
public class Main {
    public static void main(String[] args) {
        IUserRepository userRepository = new FileUserRepository();
        // Создаем двух пользователей
        User user1 = new User(1, "Пользователь 1", "пароль 1", "Пользователь Один");
        User user2 = new User(2, "Пользователь 2", "Пароль 2", "Пользователь Два");
        // Добавляем пользователей в репозиторий
        userRepository.add(user1);
        userRepository.add(user2);
        // Получаем всех пользователей и выводим их имена
        Vector<User> allUsers = userRepository.getAll();
        for (User user : allUsers) {
            System.out.println(user.getName());
        }
        // Получаем пользователя по его идентификатору и выводим его имя
        User retrievedUser = userRepository.getById(1);
        if (retrievedUser != null) {
            System.out.println("Получен пользователь: " + retrievedUser.getName());
        }
        // Получаем пользователя по его логину и выводим его имя
        User userByLogin = userRepository.getByLogin("user2");
        if (userByLogin != null) {
            System.out.println("Логин пользователя: " + userByLogin.getName());
        }
        // Обновляем имя первого пользователя и обновляем информацию в репозитории
        user1.setName("Обновление имени первого пользователя");
        userRepository.update(user1);
        // Удаляем второго пользователя из репозитория
        userRepository.remove(user2);
    }
}
