import java.io.*;
import java.util.Vector;
// Класс FileUserRepository реализует интерфейс IUserRepository для работы с пользователями
public class FileUserRepository implements IUserRepository {
    private Vector<User> users;
    private String fileName = "person.txt";

    public FileUserRepository() {
        users = new Vector<>();
        loadUsers();
    }

    @Override
    public Vector<User> getAll() {
        return users;
    }

    @Override
    public void add(User user) {
        users.add(user);
        saveUsers();
    }

    @Override
    public void remove(User user) {
        users.remove(user);
        saveUsers();
    }

    @Override
    public void update(User user) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == user.getId()) {
                users.set(i, user);
                saveUsers();
                return;
            }
        }
        System.out.println("Пользователь не найден");
    }

    @Override
    public User getById(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User getByLogin(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        return null;
    }
//  сохраняем списоки пользователей в файл
    private void saveUsers() {
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(users);
            objectOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
// загружаем списки пользователей из файла
    private void loadUsers() {
        try {
            File file = new File(fileName);
            if (file.exists()) {
                FileInputStream fileIn = new FileInputStream(fileName);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                users = (Vector<User>) objectIn.readObject();
                objectIn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
