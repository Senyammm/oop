import java.io.Serializable;
import java.util.Vector;

class User implements Serializable {
    private int id;
    private String login;
    private String password;
    private String name;

    public User(int id, String login, String password, String name) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
