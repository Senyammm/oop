import java.util.Vector;

public interface IRepository<T> {
    Vector<T> getAll();
    void add(T item);
    void remove(T item);
    void update(T item);

}
