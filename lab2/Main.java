import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<Double, Double> f = x -> Math.sin(x); // подынтегральное выражение
        double lowerBound = 0; // нижняя граница интегрирования
        double upperBound = Math.PI / 2; // верхняя граница интегрирования
        int numPoints = 10; // число точек
        double precision = 1e-6; // точность

        NumericalIntegration integration = new SimpsonIntegration(numPoints, precision);
        double integral = integration.calc(f, lowerBound, upperBound);
        System.out.println("Интеграл методом Симпсона: " + integral);

        integration = new TrapezoidalIntegration(numPoints, precision);
        integral = integration.calc(f, lowerBound, upperBound);
        System.out.println("Интеграл методом трапеций: " + integral);
    }
}
