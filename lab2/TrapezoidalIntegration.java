import java.util.function.Function;

public class TrapezoidalIntegration extends NumericalIntegration {
    public TrapezoidalIntegration(int numPoints, double precision) {
        super(numPoints, precision);
    }

    @Override
    public double calc(Function<Double, Double> f, double lowerBound, double upperBound) {
        double integral = trapezoidalRule(f, lowerBound, upperBound);
        double error = Math.abs(integral - trapezoidalRule(f, lowerBound, (lowerBound + upperBound) / 2)
                - trapezoidalRule(f, (lowerBound + upperBound) / 2, upperBound)) / 3;
        while (error > precision) {
            numPoints *= 2;
            step /= 2;
            integral = trapezoidalRule(f, lowerBound, upperBound);
            error = Math.abs(integral - trapezoidalRule(f, lowerBound, (lowerBound + upperBound) / 2)
                    - trapezoidalRule(f, (lowerBound + upperBound) / 2, upperBound)) / 3;
        }
        return integral;
    }
}
