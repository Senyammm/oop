import java.util.function.Function;

public class SimpsonIntegration extends NumericalIntegration {
    public SimpsonIntegration(int numPoints, double precision) {
        super(numPoints, precision);
    }

    @Override
    public double calc(Function<Double, Double> f, double lowerBound, double upperBound) {
        double integral = simpsonRule(f, lowerBound, upperBound);
        double error = Math.abs(integral - simpsonRule(f, lowerBound, (lowerBound + upperBound) / 2)
                - simpsonRule(f, (lowerBound + upperBound) / 2, upperBound)) / 15;
        while (error > precision) {
            numPoints *= 2;
            step /= 2;
            integral = simpsonRule(f, lowerBound, upperBound);
            error = Math.abs(integral - simpsonRule(f, lowerBound, (lowerBound + upperBound) / 2)
                    - simpsonRule(f, (lowerBound + upperBound) / 2, upperBound)) / 15;
        }
        return integral;
    }
}
