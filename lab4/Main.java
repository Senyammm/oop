public class Main {
    public static void main(String[] args) {
        Workflow workflow = new Workflow();
        VKeyboard vKeyboard = new VKeyboard(workflow);

        Key b = new Key("B");
        Key fm = new Key("Fm");
        Key f4 = new Key("F4");
        Combination combination1 = new Combination(fm.getKeyName() + "+" + f4.getKeyName());
        Combination combination2 = new Combination(b.getKeyName() + "+" + fm.getKeyName());
        vKeyboard.pressKey(b);
        vKeyboard.pressCombination(combination1);
        Key ctrl = new Key("Ctrl");
        Key alt = new Key("Alt");
        Key delete = new Key("Delete");
        vKeyboard.changeCombination(combination1, combination2);
        vKeyboard.pressCombination(combination1);
        vKeyboard.reset();
    }
}