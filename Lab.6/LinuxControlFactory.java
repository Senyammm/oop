public class LinuxControlFactory implements ControlFactory{
    public LinuxControlFactory() {
        System.out.println("Creating WindowsControlFactory");
    }
    @Override
    public Label createLabel() {
        return new LinuxLabel();
    }

    @Override
    public TextBox createTextBox() {
        return new LinuxTextBox();
    }

    @Override
    public ComboBox createComboBox() {
        return new LinuxComboBox();
    }

    @Override
    public Button createButton() {
        return new LinuxButton();
    }
}