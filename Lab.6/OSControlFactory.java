public class OSControlFactory implements ControlFactory {
    String osName = System.getProperty("os.name").toLowerCase();
    @Override
    public Label createLabel() {
        if (osName.startsWith("linux")) {
            return new LinuxLabel();
        }
        return null;
    }

    @Override
    public TextBox createTextBox() {
        if (osName.startsWith("linux")) {
            return new LinuxTextBox();
        }
        return null;
    }

    @Override
    public ComboBox createComboBox() {
        if (osName.startsWith("linux")) {
            return new LinuxComboBox();
        }
        return null;
    }

    @Override
    public Button createButton() {
        if (osName.startsWith("linux")) {
            return new LinuxButton();
        }
        return null;
    }
}