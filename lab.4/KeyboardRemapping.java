
public class KeyboardRemapping {
    public static void main(String[] args) throws InterruptedException {
        // Создаем объект клавиатуры
        Keyboard keyboard = new Keyboard();

        // Назначаем новое действие
        keyboard.appoint("Ctrl+C", "Новое действие копирование");

        // Нажимаем комбинацию клавиш
        keyboard.press("Ctrl+C");
        keyboard.rollbackAction();

        // Переназначаем комбинацию клавиш
        keyboard.appoint("Ctrl+C", "Отмена действия копирования");

        // Нажимаем комбинацию клавиш  и выполняем новое действие
        keyboard.press("Ctrl+C");
    }
}

