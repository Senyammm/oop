import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Keyboard {
//    для хранения назначенных действий для клавиш
    private Map<String, String> keyMapping;
//    для хранения истории выполненных действий
    private Stack<String> actionHistory;

    public Keyboard() {
        keyMapping = new HashMap<>();
        actionHistory = new Stack<>();
    }
//принимает клавишу и выполняет соответствующее действие из keyMapping
    public void press(String key) {
        String action = keyMapping.get(key);
        if (action != null) {
            System.out.println("Выполняется действие: " + action);
            actionHistory.push(action);
        } else {
            System.out.println("Клавиша " + key + " не имеет назначенного действия");
        }
    }
//назначает новое действие для указанной клавиши и добавляет его в keyMapping.
    public void appoint(String key, String action) {
        keyMapping.put(key, action);
    }
//отменяет последнее выполненное действие из actionHistory
    public void rollbackAction() {
        if(!actionHistory.isEmpty()) {
            System.out.println("Отменяется действие: " + actionHistory.pop());
        } else {
            System.out.println("Нет действий для отмены");
        }
    }
}
