public class KeyboardDemo {
    public static void main(String[] args) throws InterruptedException {
//    Создаем объект клавиатуры
      Keyboard keyboard = new Keyboard();
        keyboard.appoint("Ctrl+C", "Copy");
        keyboard.appoint("Ctrl+V", "Paste");
        keyboard.appoint("A", " Hello ");
        keyboard.appoint("B"," World ");

// добавление
        keyboard.press("Ctrl+C");
        Thread.sleep(1000);
        keyboard.press("Ctrl+V");
        Thread.sleep(1000);
        keyboard.press("A");
        Thread.sleep(1000);
        keyboard.press("B");
// откат последних действий назад
        keyboard.rollbackAction();
        Thread.sleep(1000);
        keyboard.rollbackAction();
    }
}