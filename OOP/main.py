import abc

class NumericalIntegration(metaclass=abc.ABCMeta):
    def __init__(self, num_points, step, accuracy):
        self.num_points = num_points
        self.step = step
        self.accuracy = accuracy

    @abc.abstractmethod
    def calc(self, expression, lower_limit, upper_limit):
        pass


class TrapezoidalIntegration(NumericalIntegration):
    def calc(self, expression, lower_limit, upper_limit):
        # Проверка, что выражение функция
        if not callable(expression):
            raise ValueError("The expression must be a callable function.")

        # Проверка корректности пределов интегрирования
        if lower_limit >= upper_limit:
            raise ValueError("The lower limit must be less than the upper limit.")

        result = 0
        x = lower_limit

        # Метод трапеций: суммируем площади трапеций
        for i in range(self.num_points):
            result += (expression(x) + expression(x + self.step)) * self.step / 2
            x += self.step

        return result


class SimpsonIntegration(NumericalIntegration):
    def calc(self, expression, lower_limit, upper_limit):
        # Проверка, что выражение функция
        if not callable(expression):
            raise ValueError("The expression must be a callable function.")

        # Проверка корректности пределов интегрирования
        if lower_limit >= upper_limit:
            raise ValueError("The lower limit must be less than the upper limit.")

        # Проверка, что количество точек четное для метода Симпсона
        if self.num_points % 2 != 0:
            raise ValueError("The number of points must be even for Simpson's rule.")

        result = 0
        x = lower_limit

        # Метод Симпсона: суммируем площади участков кривой
        for i in range(int(self.num_points / 2)):
            result += (expression(x) + 4 * expression(x + self.step) + expression(x + 2 * self.step)) * self.step / 3
            x += 2 * self.step

        return result


# Пример использования


def example_function(x):
    return x ** 2 + 2 * x - 1

# Создаем объекты для методов трапеций и Симпсона
trap_integration = TrapezoidalIntegration(500, 0.01, 1e-3)
simp_integration = SimpsonIntegration(500, 0.01, 1e-3)

# Вычисляем интегралы с помощью функции-примера
result_trap = trap_integration.calc(example_function, 0, 1)
result_simp = simp_integration.calc(example_function, 0, 1)

# Выводим результаты
print("Результат трапеций:", result_trap)
print("Результат Симпсона:", result_simp)